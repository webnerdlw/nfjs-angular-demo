var express = require('express');
var path = require('path');
var fs = require('fs');

var Docker = require('dockerode');

var app = express();

var isProduction = process.env.NODE_ENV === 'production';
var port = isProduction ? process.env.PORT : 3001;
var publicPath = path.resolve(__dirname, 'public');

// We point to our static assets
// app.use(express.static(publicPath));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// And run the server
app.listen(port, function () {
  console.log('Server running on port ' + port);
});

/**
 * GET all containers. Supply optional `all` query params
 *
 * - `GET /containers/json?all=1 HTTP/1.1` - Return all containers
 * - `GET /containers/json?all=1 HTTP/1.1` - Returns only running containers
 *
 * E.g - `curl http://localhost:3001/api/containers\?all\=0`
 */
app.get('/api/containers', function (req, res) {
  const all = (req.query.all === '1');
  new Docker().listContainers({ all }, function (err, containers) {
    res.setHeader('Cache-Control', 'no-cache');
    res.json(containers);
  });
});

/**
 * Return low-level information on the container id
 *
 * - `GET /containers/4fa6e0f0c678/json HTTP/1.1`
 * - `GET /containers/json?all=1 HTTP/1.1` - Returns only running containers
 *
 * E.g - `curl http://localhost:3001/api/containers/b194f79e4e65fc3b4da476c6ab2ce928cc2a65c9bbb685641684ef95c1093267`
 */
app.get('/api/containers/:id', function (req, res) {
  const container = new Docker().getContainer(req.params.id);
  container.inspect(function (err, data) {
    res.setHeader('Cache-Control', 'no-cache');
    res.json(data);
  });
});
